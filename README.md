Kdenlive Debian Appimage is a tool to create a .deb package for Kdenlive that uses an Appimage as the executable.

Usage:
1.  Download or clone the package
2.  run the makedeb file
3. You should now find a file called "kdenlive-appimage.deb"
